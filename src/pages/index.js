import {
  Link as ChakraLink,
  Text,
  Code,
  List,
  ListIcon,
  ListItem,
  Heading,
} from "@chakra-ui/react";
import { CheckCircleIcon, LinkIcon } from "@chakra-ui/icons";
import { Hero } from "../components/Hero";
import { Container } from "../components/Container";
import { Main } from "../components/Main";
import { DarkModeSwitch } from "../components/DarkModeSwitch";
import { CTA } from "../components/CTA";
import { Footer } from "../components/Footer";
import { Instructions } from "./instructions";



const Index = () => (
  <Container height="100%" width="100%">
    <Hero />
    <Main>
      <Heading as="h2" size="xl" isTruncated>
        Dockerizing a <i>Three.js</i> application
        <br />
        or website.
      </Heading>

      <List spacing={3} my={0}>

        <ListItem>
          <ListIcon as={CheckCircleIcon} color="green.500" />
          <ChakraLink
            isExternal
            href="https://clickonrefresh-dashboard.netlify.app"
            flexGrow={1}
            mr={2}
          >
            by clickonrefresh <LinkIcon />
          </ChakraLink>
        </ListItem>
        <ListItem>
          <ListIcon as={CheckCircleIcon} color="green.500" />
          <ChakraLink isExternal href="https://threejs.org" flexGrow={1} mr={2}>
            Threejs <LinkIcon />
          </ChakraLink>
        </ListItem>
        <ListItem>
          <ListIcon as={CheckCircleIcon} color="green.500" />
          <ChakraLink
            isExternal
            href="https://docs.pmnd.rs/home"
            flexGrow={1}
            mr={2}
          >
            react-three-fiber <LinkIcon />
          </ChakraLink>
        </ListItem>
      </List>
      <Instructions />
    </Main>

  



    <DarkModeSwitch />
    <Footer>
      <Text>Next ❤️ Chakra ❤️ Three ❤️ Docker</Text>
    </Footer>
    <CTA />
  </Container>
);

export default Index;
