import { ChakraProvider, ColorModeProvider } from "@chakra-ui/react";

import theme from "../theme";

import {
  Link as ChakraLink,
  Text,
  Code,
  List,
  ListIcon,
  ListItem,
  Heading,
  Spinner,
  Progress,
} from "@chakra-ui/react";
import { CheckCircleIcon, LinkIcon } from "@chakra-ui/icons";
import { Hero } from "../components/Hero";
import { Container } from "../components/Container";
import { Main } from "../components/Main";
import { DarkModeSwitch } from "../components/DarkModeSwitch";
import { CTA } from "../components/CTA";
import { Footer } from "../components/Footer";

export const Instructions = () => (
  <Main>
    <Text>
      Dockerizing a threejs app or website is alot easier than may seem. First,
      ensure you have met the pre-requisites:
      <br />
      <br />
      <ul>
        <li>Install Docker &amp; Docker Compose</li>
        <br />
        <li>Have an existing three.js project</li>
        <br />
        <br />
        <br />
      </ul>
      <Heading as="h3" size="lg">
        Install Docker
      </Heading>
      <br />
      <Code>
        sudo apt-get install apt-transport-https ca-certificates curl
        software-properties-common -y
        <br />
        sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo
        apt-key add -
        <br />
        sudo add-apt-repository "deb [arch=amd64]
        https://download.docker.com/linux/ubuntu focal stable"
        <br />
        sudo apt-get update
        <br />
        sudo apt install docker-ce -y
        <br />
        sudo usermod -aG docker $USER
        <br />
        newgrp docker
        <br />
      </Code>
      <br />
      <br />
      <br />
      <Heading as="h3" size="lg">
        Install Docker-Compose
      </Heading>
      <br />
      <Code>
        sudo curl -L
        "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname
        -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        <br />
        sudo chmod +x /usr/local/bin/docker-compose
        <br />
        sudo apt-get update
      </Code>
    </Text>

    <br />
    <br />
    <br />

    <Heading>Dockerize Threejs Instructions </Heading>

    <br />

    <Text>
      Now that you have docker &amp; docker-compose installed,
      <br />
      and a three.js project, you can serve your project a couple different ways
      with docker.
      <br />
      <Heading as="h4" size="md">
        <br />
        Run a 'dev' container:
      </Heading>
      <br />
      To create a docker image that is mapped to your 'src/index' path, run
      <br />
      <br />
      <Code>npm run dev</Code>
      <br />
      <br />
      Then run
      <br />
      <br />
      <Code>
        docker run -p 8822:80 -v ~/my-project/src/index:/usr/share/nginx/html -d
        nginx
      </Code>
      <br />
      <br />
      the <i>-v</i> flag mounts volumes using the full path of the development
      server folder, mapped to the users shared www html folder inside the nginx
      container.
      <br />
      The <i>-p</i> flag sets the ports binding, in this case port 80 of the
      nginx container is being forwarded to port 8822 on the host.
      <br />
      <br />
      <Heading as="h4" size="md">
        <br />
        Run a 'production' container:
      </Heading>
      <br />
      To create a docker image that is mapped to your 'build or public' path,
      run
      <br />
      <br />
      <Code>npm run start</Code>
      <br />
      <br />
      Then run
      <br />
      <br />
      <Code>
        docker run -p 8822:80 -v ~/my-project/build:/usr/share/nginx/html -d
        nginx
      </Code>
      <br />
      <br />
      the <i>-v</i> flag mounts volumes using the full path of the production
      server folder, mapped to the users shared www html folder inside the nginx
      container.
      <br />
      The <i>-p</i> flag sets the ports binding, in this case port 80 of the
      nginx container is being forwarded to port 8822 on the host.
      <br />
      <br />
      <Heading as="h4" size="md">
        <br />
        Tagging images and pushing to gitlab container registry
      </Heading>
      <br />
      <br />
      Ensure you have a gitlab repository for your project as that repo contains
      access to the container registry which you will use to push your docker
      images to.
      <br />
      Login to your gitlab account using a PAT generated in 'settings'
      <br />
      <br />
      <Code>docker login registry.gitlab.com/your-username/your-repo-name</Code>
      <br />
      <br />
      To tag a docker image, run
      <br />
      <br />
      <Code>
        docker -t registry.gitlab.com/your-username/your-repo-name:yourtagname{" "}
      </Code>
      <br />
      <br />
      Then push the image to gitlab container registry
      <br />
      <br />
      <Code>
        docker push registry.gitlab.com/your-username/your-repo-name:yourtagname
      </Code>
      <br />
      <br />
      <br />
    </Text>

    <Text> More coming soon</Text>
    <Progress size="xs" isIndeterminate />
  </Main>
);

export default Instructions;
