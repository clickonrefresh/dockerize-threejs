import { Link as ChakraLink, Button } from '@chakra-ui/react'

import { Container } from './Container'

export const CTA = () => (
  <Container
    flexDirection="row"
    position="fixed"
    bottom="0"
    width="100%"
    maxWidth="48rem"
    py={2}
  >
    <ChakraLink isExternal href="https://clickonrefresh-dashboard.netlify.app" flexGrow={1} mx={2}>
      <Button width="100%" bgGradient="linear(to-tr, teal.300,yellow.400)">
        by clickonrefresh
      </Button>
    </ChakraLink>

    <ChakraLink
      isExternal
      href="https://clickonrefresh.github.io"
      flexGrow={3}
      mx={2}
    >
      <Button width="100%" bgGradient="linear(to-tr, teal.300,yellow.400)">
        clickonrefresh 3D
      </Button>
    </ChakraLink>
  </Container>
)
